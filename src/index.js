import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import JokeTable from './JokeTable';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<JokeTable />, document.getElementById('joketable'));
registerServiceWorker();
