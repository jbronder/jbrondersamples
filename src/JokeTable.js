import { render } from 'react-dom';
import React, { Component, PropTypes } from 'react';
import SortableTable from 'react-sortable-table';
import './JokeTable.css';
window.React = require('react');

const gatherJokesUrl = '//localhost:8000/getJokes';
const sortJokesUrl = '//localhost:8000/postSortedJokes';
 
class JokeTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            requestFailed: false,
            sortField: 'id',
            sortDirection: 'asc'
        }

        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleDirectionChange = this.handleDirectionChange.bind(this);
        this.sortJokes = this.sortJokes.bind(this);
    }

    //Update state value when new field is selected.
    handleFieldChange(event){
        this.setState({sortField: event.target.value});
    }

    //Update state value when new direction is selected.
    handleDirectionChange(event){
        this.setState({sortDirection: event.target.value});
    }

    //Send the jokes to be sorted. Used fetch to remain consistent.
    sortJokes(event) {
        event.preventDefault();
        fetch(sortJokesUrl,{
            method: 'POST',
            headers: {'Accept':'application/json',
                      'Content-Type': 'application/json'},
            body: JSON.stringify({
                jokeJsonDict : this.state.jokeData,
                sortField : this.state.sortField,
                sortDirection : this.state.sortDirection})
             })
        .then(response => {
            if (!response.ok) {
                throw Error("Network request failed")
            }
            console.log(response);
            return response
        })
        .then(responseData => responseData.json())
        .then(responseData => {
            console.log(responseData);
            this.setState({
                jokeData: responseData
            })
        }, () => {
            this.setState({
                requestFailed: true
            })
        })
    }

    //Initial load of joke JSON.
    componentDidMount() {
        //Only request jokes on initial load. Used fetch to gather because it was the cleanest way I found make requests.
        if (!this.state.jokeData){
            fetch(gatherJokesUrl)
                .then(response => {
                    if (!response.ok) {
                        throw Error("Network request failed")
                    }
                    console.log(response);
                    return response
                })
                .then(responseData => responseData.json())
                .then(responseData => {
                    console.log(responseData);
                    this.setState({
                        jokeData: responseData
                    })
                }, () => {
                    this.setState({
                        requestFailed: true
                    })
                })
            }
        }
    render() {

        //If request fails let user know.
        if (this.state.requestFailed) return <p className="JokeTable_FormStyle">Failed!</p>
        //Request is still loading let user know.
        if (!this.state.jokeData) return <p className="JokeTable_FormStyle">Loading...</p>
        //Request returned successfully, display the sorting form and joke table.
        if (this.state.jokeData) {

            //Create table library's column settings, styling is incorporated into the library hence why it is not on the
            //external sheet.
            const columns = [
            {
                header: 'ID',
                key: 'id',
                headerStyle: { fontSize: '15px', backgroundColor: '#FFDAB9', width: '100px' },
                dataStyle: { fontSize: '15px', paddingRight: '5px'},
                dataProps: { className: 'align-right' },
                sortable: false
            },
            {
                header: 'TYPE',
                key: 'type',
                headerStyle: { fontSize: '15px', backgroundColor: '#FFDAB9' },
                headerProps: { className: 'align-left' },
                sortable: false
            },
            {
                header: 'SETUP',
                key: 'setup',
                headerStyle: { fontSize: '15px', backgroundColor: '#FFDAB9' },
                sortable: false
            },
            {
                header: 'PUNCHLINE',
                key: 'punchline',
                headerStyle: { fontSize: '15px', backgroundColor: '#FFDAB9' },
                sortable: false
            }
            ];

            const tableStyle = {
                backgroundColor: '#eee',
                marginLeft: 'auto',
                marginRight: 'auto'
            };

            return (
                <div id="parent">
                    <div className="JokeTable_FormStyle">
                        <form onSubmit={this.sortJokes}>
                        <label className="JokeTable_Label">
                        Field:
                        <select value={this.state.sortField} onChange={this.handleFieldChange}>
                            <option value="id">ID</option>
                            <option value="type">TYPE</option>
                            <option value="setup">SETUP</option>
                            <option value="punchline">PUNCHLINE</option>
                        </select>
                        </label>
                        <label className="JokeTable_Label">
                        Sorting Direction:
                        <select value={this.state.sortDirection} onChange={this.handleDirectionChange}>
                            <option value="asc">ASCENDING</option>
                            <option value="des">DESCENDING</option>
                        </select>
                        </label>
                        <input type="submit" value="Sort" />
                        </form>
                    </div>
                    <SortableTable
                        data={this.state.jokeData}
                        columns={columns}
                        style={tableStyle}
                     />
                </div>
            );
        }
    }
}

export default JokeTable;